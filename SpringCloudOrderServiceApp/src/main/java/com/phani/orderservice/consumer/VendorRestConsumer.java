package com.phani.orderservice.consumer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class VendorRestConsumer {
	
	//1. Autowire Discovery Client
	@Autowired
	private DiscoveryClient discoveryClient;
	
	//2. Define one operation for find process
	public String getVendorData() {
		
		//3. Goto Eureka server and get List of ServiceIntances's
		List<ServiceInstance> list = discoveryClient.getInstances("VENDOR-SERVICE");
		
		//4. read for serviceInstance at index#0 (as we have one instance now)
		ServiceInstance serviceInstance = list.get(0);
		
		//5. Read URI and add path
		String url = serviceInstance.getUri()+ "/vendor/msg";
		
		//6. Use RestTemplate
		RestTemplate rt = new RestTemplate();
		
		//7. make call and get response
		ResponseEntity<String> resp = rt.getForEntity(url, String.class);
		
		//8. return response body
		return resp.getBody();
	}
}

